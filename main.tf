module "dummy_helm_chart" {
  source = "git::https://gitlab.com/wiregate-public/terraform-modules/dummy-helm-chart"
}

resource "helm_release" "helm_sa" {
  name             = "helm-sa"
  chart            = module.dummy_helm_chart.module_path
  namespace        = var.helm_sa_namespace
  create_namespace = true
  depends_on       = [module.dummy_helm_chart]

  set {
    name  = "rawyaml"
    value = base64encode(file("${path.module}/manifests/helm-serviceaccount-role-rolebinding-secret.yaml"))
  }
}
