# Helm Service Account
Service Account for deploying applications via CI/CD into Kubernetes cluster.

This modules installs following manifests inside of the specified namespace:
 - ServiceAccount;
 - Role;
 - RoleBinding;
 - Secret;
